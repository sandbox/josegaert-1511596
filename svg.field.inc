<?php

/**
 * @file
 * Implement an svg field, based on the file module's file field.
 *
 * The image module in core has also been used.
 */

/**
 * Implements hook_field_info().
 */
function svg_field_info() {
  return array(
    'svg' => array(
      'label' => t('SVG Image'),
      'description' => t('This field stores the code of a SVG image.'),
      'default_widget' => 'svg',
      'default_formatter' => 'svg',
      'settings' => array(
        'uri_scheme' => 'temporary',
        'default_image' => 0,
      ),
      'instance_settings' => array(
        'file_extensions' => 'svg',
        'file_directory' => '',
        'max_filesize' => '',
        'title_field' => 0,
        'desc_field' => 0,
        'default_image' => 0,
      ),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function svg_field_settings_form($field, $instance) {
  $defaults = field_info_field_settings($field['type']);
  $settings = array_merge($defaults, $field['settings']);

  $form['default_image'] = array(
    '#title' => t('Default image'),
    '#type' => 'managed_file',
    '#description' => t('If no svg is uploaded, this svg will be shown on display.'),
    '#default_value' => $field['settings']['default_image'],
    //'#upload_location' => $field['settings']['uri_scheme'] . '://default_images/',
  );

  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function svg_field_instance_settings_form($field, $instance) {
  $form = file_field_instance_settings_form($field, $instance);

  // Remove the description option.
  unset($form['file_directory']);
  unset($form['description_field']);

  // Add title and desc configuration options.
  $form['title_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Title</em> field'),
    '#default_value' => $instance['settings']['title_field'],
    // TODO: Is this true?
    '#description' => t('The title attribute is used as a tooltip when the mouse hovers over the image.'),
    '#weight' => 11,
  );
  $form['desc_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Alt</em> field'),
    '#default_value' => $instance['settings']['desc_field'],
    '#description' => t('The desc attribute may be used by search engines, screen readers, and when the image cannot be loaded.'),
    '#weight' => 10,
  );

  // Add the default image to the instance.
  $form['default_image'] = array(
    '#title' => t('Default image'),
    '#type' => 'managed_file',
    '#description' => t("If no image is uploaded, this image will be shown on display and will override the field's default image."),
    '#default_value' => $instance['settings']['default_image'],
    //'#upload_location' => $field['settings']['uri_scheme'] . '://default_images/',
  );

  return $form;
}

/**
 * Implements hook_field_is_empty().
 */
function svg_field_is_empty($item, $field) {
  return empty($item['svg']);
}

/**
 * Implements hook_field_widget_info().
 */
function svg_field_widget_info() {
  return array(
    'svg' => array(
      'label' => t('Svg'),
      'field types' => array('svg'),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function svg_field_widget_settings_form($field, $instance) {
  $form = image_field_widget_settings_form($field, $instance); 

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function svg_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Add display_field setting to field because file_field_widget_form() assumes it is set.
  $field['settings']['display_field'] = FALSE;

  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);

  foreach (element_children($elements) as $delta) {
    $elements[$delta]['#upload_validators']['file_validate_extensions'][0] = 'svg';

    // Add all extra functionality provided by the image widget.
    $elements[$delta]['#process'][] = 'svg_field_widget_process';
    $elements[$delta]['#pre_render'] = array('svg_widget_pre_render');
  }

  if (!empty($items[$delta]['svg'])) {
    unset($elements[$delta]['#description']);
  }

  if ($field['cardinality'] != 1) {
    $elements['#theme'] = 'svg_widget_multiple';  
  }
  return $elements;
}

/**
 * An element #process callback for the svg field type.
 *
 * @todo: Expands the svg type to include the title and desc fields.
 */
function svg_field_widget_process($element, &$form_state, $form) {
  $element['#value']['fid'] = $element['fid']['#value'];

  $instance = field_widget_instance($element, $form_state);

  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];

  $element['#theme'] = 'svg_widget';
  $element['#attached']['css'][] = drupal_get_path('module', 'svg') . '/svg.css';

  // Add the svg preview.
  $variables['style_name'] = $widget_settings['preview_image_style'];

  if (($element['#file'] || isset($element['#value']['svg'])) && $widget_settings['preview_image_style']) {
    $source = implode('_', $element['#parents']);

    // Determine svg dimensions.
    if (!isset($element['#value']['svg'])) {
      $info = svg_file_get_info($source,$element['#file']->uri);
    }
    else {
      $info = svg_get_info($source,$element['#value']['svg']);
    }

    if (is_array($info)) {
      $variables['width'] = $info['width'];
      $variables['height'] = $info['height'];
      $variables['svg'] = $info['svg'];
    
      $element['preview'] = array(
        '#type' => 'markup',
        '#markup' => theme('svg_style', $variables),
      );

      // Store everything in the form so the file doesn't have to be accessed ever again.

      $element['width'] = array(
        '#type' => 'hidden',
        '#value' => $variables['width'],
      );
      $element['height'] = array(
        '#type' => 'hidden',
        '#value' => $variables['height'],
      );
      $element['svg'] = array(
        '#type' => 'hidden',
        '#value' => $variables['svg'],
      );
    }
    //else {
    //  $variables['width'] = $variables['height'] = $variables['svg'] = NULL;
    //}

    unset($element['filename']);

    foreach (array('upload_button', 'remove_button') as $key) {
      // Remove file_field_widget_submit.
      array_pop($element[$key]['#submit']);
      //unset($element[$key]['#submit'][1]);
      $element[$key]['#submit'][] = 'svg_field_widget_submit';
    }
  }
  return $element;
}


/**
 * Replaces file_field_widget_submit.
 *
 * Copies the file_field_widget_submit entirely but replaces 
 * @code 
 *   if (!$submitted_value['fid']) { 
 * @endcode
 * with
 * @code
 *   if (is_null($submitted_value['fid'])) {
 * @endcode
 *
 * @see image_field_widget_submit()
 */
function svg_field_widget_submit($form, &$form_state) {
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -2);
  drupal_array_set_nested_value($form_state['input'], $parents, NULL);

  $button = $form_state['triggering_element'];

  // Go one level up in the form, to the widgets container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];

  $submitted_values = drupal_array_get_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2));
  foreach ($submitted_values as $delta => $submitted_value) {
    // Changed line.
    if (is_null($submitted_value['fid']) || !isset($submitted_value['svg']) 
      || !$submitted_value['svg'] ) {
      unset($submitted_values[$delta]);
    }
  }

  // Re-index deltas after removing empty items.
  $submitted_values = array_values($submitted_values);

  // Update form_state values.
  drupal_array_set_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2), $submitted_values);

  // Update items.
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $field_state['items'] = $submitted_values;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
}

/**
 * Replaces theme_file_widget_multiple.
 *
 * Changed only one line:
 * @code
 * if ($widget['#file'] == FALSE) {
 * @endcode
 * to
 * @code
 * if (!isset($widget['svg'])) {
 * @endcode
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 *
 * @ingroup themeable
 * @see theme_file_widget_multiple()
 */
function theme_svg_widget_multiple($variables) {
  $element = $variables['element'];

  // Special ID and classes for draggable tables.
  $weight_class = $element['#id'] . '-weight';
  $table_id = $element['#id'] . '-table';

  // Build up a table of applicable fields.
  $headers = array();
  $headers[] = t('File information');
  if ($element['#display_field']) {
    $headers[] = array(
      'data' => t('Display'),
      'class' => array('checkbox'),
    );
  }
  $headers[] = t('Weight');
  $headers[] = t('Operations');

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $widgets = array();
  foreach (element_children($element) as $key) {
    $widgets[] = &$element[$key];
  }
  usort($widgets, '_field_sort_items_value_helper');

  $rows = array();
  foreach ($widgets as $key => &$widget) {
    // Save the uploading row for last.
    // Changed line.
    if (!isset($widget['svg'])) {
      $widget['#title'] = $element['#file_upload_title'];
      $widget['#description'] = $element['#file_upload_description'];
      continue;
    }

    // Delay rendering of the buttons, so that they can be rendered later in the
    // "operations" column.
    $operations_elements = array();
    foreach (element_children($widget) as $sub_key) {

      if (isset($widget[$sub_key]['#type']) && $widget[$sub_key]['#type'] == 'submit') {
        hide($widget[$sub_key]);
        $operations_elements[] = &$widget[$sub_key];
      }
    }

    // Delay rendering of the "Display" option and the weight selector, so that
    // each can be rendered later in its own column.
    if ($element['#display_field']) {
      hide($widget['display']);
    }
    hide($widget['_weight']);

    // Render everything else together in a column, without the normal wrappers.
    $widget['#theme_wrappers'] = array();
    $information = drupal_render($widget);

    // Render the previously hidden elements, using render() instead of
    // drupal_render(), to undo the earlier hide().
    $operations = '';
    foreach ($operations_elements as $operation_element) {
      $operations .= render($operation_element);
    }
    $display = '';
    if ($element['#display_field']) {
      unset($widget['display']['#title']);
      $display = array(
        'data' => render($widget['display']),
        'class' => array('checkbox'),
      );
    }
    $widget['_weight']['#attributes']['class'] = array($weight_class);
    $weight = render($widget['_weight']);

    // Arrange the row with all of the rendered columns.
    $row = array();
    $row[] = $information;
    if ($element['#display_field']) {
      $row[] = $display;
    }
    $row[] = $weight;
    $row[] = $operations;
    $rows[] = array(
      'data' => $row,
      'class' => isset($widget['#attributes']['class']) ? array_merge($widget['#attributes']['class'], array('draggable')) : array('draggable'),
    );
  }

  drupal_add_tabledrag($table_id, 'order', 'sibling', $weight_class);

  $output = '';
  $output = empty($rows) ? '' : theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= drupal_render_children($element);
  return $output;
}

/**
 * Replaces file_managed_file_pre_render.
 * 
 * @see file_managed_file_pre_render()
 */
function svg_widget_pre_render($element) {
  // If we already have a file, we don't want to show the upload controls.
  if (!empty($element['svg'])) {
    $element['upload']['#access'] = FALSE;
    $element['upload_button']['#access'] = FALSE;
    $element['remove_button']['#access'] = TRUE;
  }
  // If we don't already have a file, there is nothing to remove.
  else {
    $element['upload']['#access'] = TRUE;
    $element['upload_button']['#access'] = TRUE;
    $element['remove_button']['#access'] = FALSE;
  }
  return $element;
}


/**
 * Replaces theme_image_widget.
 *
 * @ingroup themeable
 * @see theme_image_widget()
 */
function theme_svg_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="svg-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="svg-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="svg-widget-data">';
  
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Implements hook_field_formatter_info().
 */
function svg_field_formatter_info() {
  return array(
    'svg' => array(
      'label' => t('Default'),
      'field types' => array('svg'),
      'settings' => array(
        'image_style' => '', 
        'image_link' => '',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function svg_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  
  $element = image_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  unset($element['image_link']);

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function svg_field_formatter_settings_summary($field, $instance, $view_mode) {
  return image_field_formatter_settings_summary($field, $instance, $view_mode);
}

/**
 * Implements hook_field_formatter_view().
 */
function svg_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) { 

  $element = image_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
  foreach ($items as $delta => $item) {
    $element[$delta]['#theme'] = 'svg_formatter';
  }  

  return $element;
}
