(function ($) {
/**
 * Open svg-edit when the button is clicked.
 */
Drupal.behaviors.svgedit = {
  attach: 
    function (context, settings) {
      // Configuration of the svg-edit button.
      $('.svgedit-open', context).removeClass('form-button-disabled').removeAttr('disabled').click(function (e) {
        var button = $(this);
        /**         
         * This button serves only to activate the svg-editor. Prevent the standard behavior of POST.
         * http://fuelyourcoding.com/jquery-events-stop-misusing-return-false/
         */
        e.preventDefault();
        var name = $(this).attr('name');
        svgTextareaSelector = $('[name="' + name + '[svg]' +'"]');

        // If svgTextarea does not exist, create it and try again and remember that we have to upload the image afterwards.
        if (!svgTextareaSelector.length) {
          var upload = true;
          $('[name="' + name + '[display]"]').after('<input type="hidden" name="'+ name + '[svg]">');
          svgTextareaSelector = $('[name="' + name +'[svg]"]');
        }

        // Open and configure svg-editor.
        // TODO:  Add width=500,height=550 to next line
        var editor = window.open(Drupal.settings.svgedit.path + "?initStroke[width]=2&dimensions=640,240", 'Edit SVG Image', 'status=1, resizable=1, scrollbars=1'); //TODO: replace "?ini...".
        editor.addEventListener("load", function() {
          editor.svgEditor.setCustomHandlers({
            'save': function(window,svg){
              editor.svgEditor.setConfig({no_save_warning: true});
              window.opener.postMessage(svg, window.location.protocol + '//' + window.location.host);
              window.close();
            }
          });

          editor.svgEditor.randomizeIds();
          if(svgTextareaSelector.val()) { // Check if the svg input field is not empty.
            editor.svgEditor.loadFromString( svgTextareaSelector.val() );
          }
        }, true);
      editor.focus();

        var my_loc = window.location.protocol + '//' + window.location.host;
        window.addEventListener("message", function(event) { 
          if (event.origin !== my_loc)  
            return;
          svgTextareaSelector.val(event.data);

          // Rerender the picture.
          if (upload) {
            button.closest('div.svg-widget-data').find('[id$="-upload-button"]').trigger('mousedown');
          } 
          else {
            svgTextareaSelector.parent().prev().find('.original').children(':first').replaceWith(event.data);
          }
        });  
      }); 
    }

};

}(jQuery));

