<?php
/**
 * @file
 * Adds a open svg-editor button to the svg field.
 */

/**
 * Implements hook_field_widget_form_alter().
 */
function svgedit_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['instance']['widget']['type'] == 'svg') {
    
    // Attach javascript.
    if(!$form_state['rebuild']) {
      $element['#attached']['js'][] = array( 
        'data' => drupal_get_path('module', 'svgedit') . '/svgedit.js',
        'type' => 'file'
      );
      $element['#attached']['js'][] = array(
        'data' => array( 'svgedit' => array( 'path' => $GLOBALS['base_path'] . libraries_get_path('svgedit') . '/svg-editor.html')),
        'type' => 'setting'
      );
    }
    
    // Add new button to the widget.
    foreach (element_children($element) as $delta) {
      // Add all extra functionality provided by the image widget.
      $element[$delta]['#process'][] = 'svgedit_field_widget_process';
    }
  }
}

/**
 * An element #process callback for the svgedit field type.
 */
function svgedit_field_widget_process($element, &$form_state, $form) {
  $element['svgedit_button'] = array(
    '#name' =>  $element['#parents'][0] . '[' . implode('][', array_slice($element['#parents'] , 1)) . ']',
    '#type' => 'submit',
    '#value' => t('Open Svg-edit'),
    '#disabled' => TRUE,
    '#attributes' => array('class' => array('svgedit-open')),
  );
  return $element;
}

/**
 * Implements hook_libraries_info.
 *
 * @todo: remove this.
 */
function svgedit_libraries_info() {
  $libraries['svgedit'] = array(
    'name' => 'svg-edit',
    'vendor url' => 'http://code.google.com/p/svg-edit/',
    'version arguments' => array(
      'file' => 'svg-editor.html',
      'pattern' => '@SVG-edit\sv([0-9a-zA-Z\-\.]+)@',
      'lines' => '2000',
      'cols' => '2000',
    ),
    'versions' => array(
      '2.5.1' => array(
        'download url' => 'http://code.google.com/p/svg-edit/downloads/list',
        'variants' => array(
          'source' => array(
            'files' => array(
              'js' => array(
                'jgraduate/jquery.jgraduate.js',
                'svgicons/jquery.svgicons.js',
                'jquerybbq/jquery.bbq.min.js',
                'spinbtn/JQuerySpinBtn.js',
                'svgcanvas.js',
                'svg-editor.js',
                'locale/locale.js',
              ),
            ),
          ),
          'minified' => array(
            'files' => array(
              'js' => array(
                'jgraduate/jquery.jgraduate.min.js',
                'svgicons/jquery.svgicons.min.js',
                'jquerybbq/jquery.bbq.min.js',
                'spinbtn/JQuerySpinBtn.min.js',
                'svgcanvas.min.js',
                'svg-editor.min.js',
                'locale/locale.min.js',
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_menu().
 */
function svgedit_menu() {
  $items = array();

  $items['admin/config/media/svgedit'] = array(
    'title' => 'Svg-Edit',
    'description' => 'Configuration for the Svg-edit module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('svgedit_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Page callback: Current posts settings
 *
 * @see svgedit_menu()
 */
function svgedit_form($form, &$form_state) {
  $form['svgedit_host_google'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Svg-Edit hosted by google.'),
    '#description' => t('Every time your uploads Svg-Edit it uses around 1Mb of traffic. Check this to use the Svg-Edit hosted on the Svg-Edit project page at code.google.com.'),
  );
  $form['svgedit_version'] = array(
    '#type' => 'select',
    '#title' => t('Version'),
    '#options' => array(
      0 => t('2.5.1'),
      1 => t('trunk (latest)'),
    ),
    '#default_value' => 1,
    '#description' => t('Select which version to use from Google.'),
  );

  return system_settings_form($form);
}
