(function ($) {

  Drupal.behaviors.nounprojectfield = {

    attach: function(context, settings) {

      $('.field-widget-nounproject-browse .form-text').each(function(){

        $('<div class="results"></div>').insertAfter(this);

        $('<a class="button">' + Drupal.t('Search') + '</a>')
          .insertAfter(this)
          .click(function() {
          
            var text = $(this).siblings('input.form-text').val();
            var resultsDiv = $(this).siblings('div.results');

            if (text.length > 0) {

              $.getJSON('nounproject/search/?search=' + encodeURIComponent(text), function(data){

                resultsDiv.empty();
 
                for (var i in data.svg) {
                
                  var result = $('<div class="result">' + data.svg[i] + '<h4>' + data.trans[i] + '</h4></div>');

                  result.click(nounprojectResultClick);

                  resultsDiv.append(result);
                
                } // for
  
              }); // getJSON

            } // if

        }); // click
      
      }); // each

    } // attach

  } // Drupal.behaviors.nounprojectfield

  function nounprojectResultClick() {

    var input = $(this).parents('.form-item').next('input[type=hidden]');
    var svg = $(this).find('svg').clone();
    var svgDiv = $('<div>').append(svg);

    input.val(svgDiv.html());

  } // nounprojectResultClick

})(jQuery);
