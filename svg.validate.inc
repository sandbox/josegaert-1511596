<?php
/**
 * @file
 * Holds the function that validates the svg code and clears it from xss attacks.
 */

/**
 * Validates svg code to prevent xss attacs
 *
 * The code is inspired on the code of the mediawiki project, the content
 * management system that is used by Wikipedia. @link www.mediawiki.com
 * @endlink. The function is called checkSvgScriptCallback, all improvments 
 * found here should also be uploaded there. Better even, a whitelest filter
 * should be constructed for both projects.
 *
 * @param DOMDocument $svg
 *   A DOMDocument object that holds the svg code.
 *
 * @param 
 *
 * @return
 *   TRUE if the svg passes the validation, FALSE otherwise.
 *
 * @todo Replace this with a whitelist filter!
 */
function svg_validate($source, $svg) {
  $errors = array();
  $elements = $svg -> getElementsByTagName('*');

  foreach ($elements as $elements => $element) {

    // Check for elements that can contain javascript.
    if( $element->localName == 'script' ) {
      $errors[] = t('Found script element in uploaded file.');
    }

    // <svg xmlns="http://www.w3.org/2000/svg"> 
    //   <handler xmlns:ev="http://www.w3.org/2001/xml-events" ev:event="load">
    //     alert(1)
    //   </handler> 
    // </svg>
    if( $element->localName == 'handler' ) {
      $errors[] = t("Found script element in uploaded file.");
    }

    // SVG reported in Feb '12 that used xml:stylesheet to generate 
    // javascript block.
    if( $element->localName == 'stylesheet' ) {
      $errors[] = t('Found script element in uploaded file.');
    }

    foreach( $element -> attributes as $attrib => $value ) {
      $stripped = $attrib;
      $value = strtolower($value->value);

      if( substr( $stripped, 0, 2 ) == 'on' ) {
        $errors[] = t('Found event-handler attribute in uploaded file.');
      }

      // Href with javascript target.
      if( $stripped == 'href' && strpos( strtolower( $value ), 'javascript:' ) !== false ) {
        $errors[] = t('Found script in href attribute in uploaded file.');
      }

      // Href with embeded svg as target.
      if( $stripped == 'href' && preg_match( '!data:[^,]*image/svg[^,]*,!sim', $value ) ) {
        $errors[] = t('Found href to embedded svg in uploaded file.');
      }

      // Href with embeded (text/xml) svg as target.
      if( $stripped == 'href' && preg_match( '!data:[^,]*text/xml[^,]*,!sim', $value ) ) {
        $errors[] = t('Found href to embedded svg in uploaded file.');
      }

      // Use set/animate to add event-handler attribute to parent.
      if( ( $element->localName == 'set' || $element->localName == 'animate' ) && $stripped == 'attributename' && substr( $value, 0, 2 ) == 'on' ) {
        $errors[] = t('Found svg setting event-handler attribute in uploaded file.');
      }

      // Use set to add href attribute to parent element.
      if( $element->localName == 'set' && $stripped == 'attributename' && strpos( $value, 'href' ) !== false ) {
        $errors[] = t('Found svg setting href attribute in uploaded file.');
      }

      // Use set to add a remote / data / script target to an element.
      if( $element->localName == 'set' && $stripped == 'to' &&  preg_match( '!(http|https|data|script):!sim', $value ) ) {
        $errors[] = t('Found svg setting attribute in uploaded file.');
      }

      // Use handler attribute with remote / data / script.
      if( $stripped == 'handler' &&  preg_match( '!(http|https|data|script):!sim', $value ) ) {
        $errors[] = t('Found svg setting handler with remote/data/script in uploaded file.');
      }

      // Use CSS styles to bring in remote code.
      // Catch url("http:..., url('http:..., url(http:..., but not url("#..., url('#..., url(#....
      if( $stripped == 'style' && preg_match_all( '!((?:font|clip-path|fill|filter|marker|marker-end|marker-mid|marker-start|mask|stroke)\s*:\s*url\s*\(\s*["\']?\s*[^#]+.*?\))!sim', $value, $matches ) ) {
        foreach ($matches[1] as $match) {
          if (!preg_match( '!(?:font|clip-path|fill|filter|marker|marker-end|marker-mid|marker-start|mask|stroke)\s*:\s*url\s*\(\s*(#|\'#|"#)!sim', $match ) ) {
            $errors[] = t('Found svg setting a style with remote url in uploaded file.');
          }
        }
      }
    }

    // image filters can pull in url, which could be svg that executes scripts
    if( $element->localName == 'image' && $stripped == 'filter' && preg_match( '!url\s*\(!sim', $value ) ) {
      $errors[] = t('Found image filter with url in uploaded file.');
    }
  }

return $errors;

  /*  
  foreach ($items as $delta => $item) {
    // Check if there is something filled in.
    if (empty($item['svg'])) {
      continue;
    }

    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($item['svg']);

    // Check if the svg' xml structure is valid.
    libxml_use_internal_errors(true);

    $error = libxml_get_errors();

    if (! empty($error)) { //&& $error[0]->level > 3) { // TODO: Should I leave the level checking out?
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'svg_xml_not_well_formed',
        'message' => t('The SVG code is not well formed: @error', array('@error' => $error[0]->message)),
      );
      continue;
    }

    // Check if the SVG is valid.
    $creator = new DOMImplementation();
    $doctype = $creator->createDocumentType('svg', '-//W3C//DTD SVG 1.1//EN', 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd');
    $newDoc = $creator->createDocument(null, null, $doctype);
    $newDoc->encoding = 'utf-8';

    $oldNode = $doc->firstChild; //$doc->getElementsByTagName('svg')->item(0);
    $newNode = $newDoc->importNode($oldNode, true);
    $newDoc->appendChild($newNode);

    if (!$newDoc->validate()) {
    
      $error = libxml_get_errors();
      //dpm($error);

      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'svg_code_invalid',
        'message' => t('There are irregularities in the svg code.'),
      );
    }
  }
 */
}
