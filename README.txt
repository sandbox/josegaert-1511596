-- SUMMARY --

The Svg module introduces a new field that makes handling svg images the same
like handling raster images for the end user. Svg uses many parts of the Drupal
core image and file modules as its building blocks.

The Svgedit module extends the Svg module so that svg images can be edited with
the (open source, web based) svg-edit editor.
See https://code.google.com/p/svg-edit/ for more information.

-- REQUIREMENTS --

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* To install the svg-edit library download the stable 2.5.1 version from the 
  svg-edit website and place the content in /sites/all/libraries/svgedit.

* You can also download the experimental version of svg-edit with following command:
svn checkout http://svg-edit.googlecode.com/svn/trunk/editor/ svgedit
Then change the name of the folder to svgedit with the command:
mv svg-edit-read-only svgedit

* One can also install svg-edit automatic with drush and the svgedit.make file.
  The make file will by default install the trunk version from svg-edit.  

-- CONFIGURATION --

*

-- CUSTOMIZATION --

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Jo Segaert (josegaert) - http://drupal.org/user/1549944

This project has been sponsored by:
* HoGent or The University College of Ghent
  Visit http://hogent.be for more information.
  
